package com.kafka.producer.service;

import com.kafka.producer.entity.Eligibility;
import com.kafka.producer.model.EligibilityRequest;
import com.kafka.producer.repository.EligibilityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class EligibilityConsumerService {

     private Logger logger = LoggerFactory.getLogger(EligibilityConsumerService.class);

     @Autowired
     private EligibilityRepository repository;

     @KafkaListener(topics = "${kafka.producer.topicName}")
     public void persistEligibility(EligibilityRequest request) {

          logger.info("Message received {}", request);
          Eligibility eligibility = new Eligibility(request.getSalary(),request.getGoldAsset(),request.getPreviousLoanAmt()
                                                    ,request.isLoanTaken());
          repository.save(eligibility);
          logger.info("Request persist in DB");
     }

}
