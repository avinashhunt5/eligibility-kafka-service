package com.kafka.producer.controller;

import com.kafka.producer.model.EligibilityRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/kafka")
public class RestKafkaController {

    private Logger LOGGER = LoggerFactory.getLogger(RestKafkaController.class);

    @Value("${kafka.producer.topicName}")
    private String topicName;

    @Autowired
    private KafkaTemplate<String, EligibilityRequest> kafkaTemplate;

    @PostMapping(value = "/validateEligibility")
    ResponseEntity<String> validateEligibility(@RequestBody EligibilityRequest eligibilityRequest) {
        LOGGER.info("Request received to send to check eligibility over kafka topic");

        ListenableFuture<SendResult<String, EligibilityRequest>> future = kafkaTemplate.send(topicName, eligibilityRequest);

        future.addCallback(new ListenableFutureCallback<SendResult<String, EligibilityRequest>>() {

            @Override
            public void onSuccess(SendResult<String, EligibilityRequest> result) {
                LOGGER.info("Sent message = {} with offset= {}" , eligibilityRequest, result.getRecordMetadata().offset());
            }

            @Override
            public void onFailure(Throwable ex) {
                LOGGER.error("Unable to send message={} due to : " ,ex.getMessage());
            }
        });
        return ResponseEntity.ok("success");
    }
}
