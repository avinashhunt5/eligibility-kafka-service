package com.kafka.producer.repository;

import com.kafka.producer.entity.Eligibility;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EligibilityRepository extends CrudRepository<Eligibility, Long> {
}
