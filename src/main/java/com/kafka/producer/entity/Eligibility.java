package com.kafka.producer.entity;

import javax.persistence.*;

@Entity
@Table
public class Eligibility {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name="salary")
    private double salary;

    @Column(name="goldAsset")
    private double goldAsset;

    @Column(name="previousLoanAmt")
    private double previousLoanAmt;

    @Column(name="loanTaken")
    private boolean loanTaken;

    public Eligibility(double salary, double goldAsset, double previousLoanAmt, boolean loanTaken) {
        this.salary = salary;
        this.goldAsset = goldAsset;
        this.previousLoanAmt = previousLoanAmt;
        this.loanTaken = loanTaken;
    }

}
